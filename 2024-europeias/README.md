# 2024-europeias

## References

- https://www.rtp.pt/noticias/politica/europeias-partidos-aceitam-proposta-das-televisoes-para-debates-a-quatro_n1570038
- https://sicnoticias.pt/pais/2024-05-12-eleicoes-europeias-primeiro-debate-esta-segunda-feira-na-sic-3cfd39e5
- https://rr.sapo.pt/noticia/politica/2024/05/13/conheca-o-calendario-dos-debates-televisivos-para-as-europeias/378151/
- https://www.rtp.pt/programa/tv/p45741/e1
- https://www.rtp.pt/programa/tv/p45741/e2
- https://tvi.iol.pt/noticias/debates/calendario/quatro-candidatos-50-minutos-e-em-sinal-aberto-vao-ser-assim-os-debates-para-as-europeias/20240508/663bb4a6d34e049892209c1e:
  - "Todos os debates serão transmitidos nas televisões generalistas, tendo 50 minutos cada, com uma tolerância até aos 55 minutos."
  - "Todos os quatro debates têm hora de começo prevista entre as 20:30 e as 21:00, nos horários de maior audiência das três televisões."
- https://github.com/spatie/icalendar-generator/blob/2.6.2/src/Components/Event.php#L128
- https://www.cne.pt/content/eleicoes-para-o-parlamento-europeu-2024
- https://eco.sapo.pt/2024/05/09/debates-das-europeias-arrancam-na-segunda-feira-veja-o-calendario/
