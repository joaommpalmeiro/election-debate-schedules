# Notes

## Snippets

- https://github.com/adamgibbons/ics/tree/v3.7.2?tab=readme-ov-file#api
  - "To create an **all-day** event, pass only three values (`year`, `month`, and `date`) to the `start` and `end` properties. The date of the `end` property should be the day _after_ your all-day event. For example, in order to create an all-day event occuring on October 15, 2018:"

```js
const eventAttributes = {
  start: [2018, 10, 15],
  end: [2018, 10, 16],
};
```
