# 2024-legislativas

## References

- https://www.rtp.pt/noticias/eleicoes-legislativas-2024/eleicoes-legislativas-o-calendario-dos-debates-televisivos_i1547247
- https://en.wikipedia.org/wiki/ISO_8601: `2024-02-03T07:19:36Z`
- https://en.wikipedia.org/wiki/Coordinated_Universal_Time
- https://en.wikipedia.org/wiki/List_of_UTC_offsets: `UTC±00:00, Z`
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/toISOString
- https://www.rtp.pt/noticias/politica/legislativas-conheca-o-calendario-dos-debates-eleitorais_n1543198
- https://www.publico.pt/2024/01/13/politica/noticia/legislativas-debates-televisivos-arrancam-5-fevereiro-pedro-nuno-montenegro-fecham-19-2076748:
  - "Todos os debates terão uma duração de 25 a 30 minutos, com excepção do de Pedro Nuno Santos e Luís Montenegro, que terá cerca de 75 minutos."
- https://www.dn.pt/2750909249/debates-comecam-a-5-de-fevereiro-ps-e-il-no-arranque/:
  - "À exceção do debate entre Pedro Nuno e Montenegro (que terá 75 minutos), todos os embates terão uma duração estipulada entre os 25 e os 30 minutos."
- https://www.rtp.pt/noticias/politica/rtp-vai-transmitir-12-debates-antes-das-legislativas_v1543326
- https://pt.wikipedia.org/wiki/Elei%C3%A7%C3%B5es_legislativas_portuguesas_de_2024
- https://pt.wikipedia.org/wiki/Elei%C3%A7%C3%B5es_legislativas_portuguesas_de_2024#Debates
- https://www.rtp.pt/tv/
- https://www.rtp.pt/rtp3
- https://en.wikipedia.org/wiki/Television_channel
- https://en.wikipedia.org/wiki/Political_party
- https://en.wikipedia.org/wiki/2024_Portuguese_legislative_election
- https://en.wikipedia.org/wiki/ICalendar
- https://www.rtp.pt/programa/tv/p45239
- https://pt.wikipedia.org/wiki/Debates_para_as_elei%C3%A7%C3%B5es_legislativas_portuguesas_de_2024
- https://www.rtp.pt/programa/tv/p45240
- https://www.rtp.pt/programa/tv/p45241
