# Notes

- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/getTime
- https://github.com/jkbrzt/rrule (JavaScript)
- https://dateutil.readthedocs.io/en/latest/rrule.html (Python)
- https://icalendar.org/
- https://icalendar.org/validator.html
- https://github.com/adamgibbons/ics/blob/master/package.json

## iCalendar packages

### Python

- https://github.com/collective/icalendar
- https://github.com/ics-py/ics-py

### JavaScript

- https://github.com/Neuvernetzung/ts-ics
- https://github.com/adamgibbons/ics
- https://github.com/sebbo2002/ical-generator

### PHP

- https://github.com/spatie/icalendar-generator

## Snippets

```json
{
  "data": "2024-02-05T21:00:00Z",
  "partidos": ["", ""],
  "canais": [""]
}
```

```json
{
  "start": "2024-02-05T21:00:00Z",
  "end": "2024-02-05T21:30:00Z",
  "parties": ["", ""],
  "channels": [""]
}
```
