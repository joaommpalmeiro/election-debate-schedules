# try-table-transformer

```bash
npx degit microsoft/table-transformer#16d124f616109746b7785f03085100f1f6247575 table_transformer
```

On macOS, delete the `cudatoolkit==11.8.0` dependency from the `table_transformer/environment.yml` file.

```bash
conda env create -f table_transformer/environment.yml
```

```bash
conda activate tables-detr
```

```bash
curl -L -o table_transformer/pubtables1m_detection_detr_r18.pth https://huggingface.co/bsmock/tatr-pubtables1m-v1.0/resolve/main/pubtables1m_detection_detr_r18.pth
```

```bash
curl -L -o table_transformer/pubtables1m_structure_detr_r18.pth https://huggingface.co/bsmock/tatr-pubtables1m-v1.0/resolve/main/pubtables1m_structure_detr_r18.pth
```

```bash
cp ../try-easyocr/table-transformer-tokens.json table_transformer/src
```

```bash
cp 01.py table_transformer/src
```

```bash
cd table_transformer/src && python 01.py
```

```bash
cd ../.. && cp 01.py table_transformer/src
```
