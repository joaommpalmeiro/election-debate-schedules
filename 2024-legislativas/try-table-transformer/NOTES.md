# Notes

- https://github.com/microsoft/table-transformer
- https://github.com/microsoft/table-transformer/blob/16d124f616109746b7785f03085100f1f6247575/docs/INFERENCE.md
- https://github.com/microsoft/table-transformer/blob/16d124f616109746b7785f03085100f1f6247575/environment.yml
- https://github.com/CompVis/stable-diffusion/issues/309: `Failure creating conda env on OSX amd64 - ResolvePackageNotFound: cudatoolkit=11.3`
- https://github.com/SuleyNL/Extractable
- https://developer.nvidia.com/nvidia-cuda-toolkit-11_3_0-developer-tools-mac-hosts
- https://www.digitalocean.com/community/tutorials/workflow-downloading-files-curl#step-3-following-redirects
- https://www.rtp.pt/noticias/pais/eleicoes-legislativas-o-calendario-dos-debates-televisivos_i1547247
- https://github.com/pytorch/pytorch/issues/86152
- https://github.com/microsoft/table-transformer/issues/121
- https://github.com/JaidedAI/EasyOCR/issues/1039
- https://support.apple.com/en-mt/guide/preview/prvw625a5b2c/11.0/mac/12.0
