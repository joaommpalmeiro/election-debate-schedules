import json

from inference import TableExtractionPipeline
from PIL import Image

DET_CONFIG = "detection_config.json"
DET_MODEL = "../pubtables1m_detection_detr_r18.pth"

STR_CONFIG = "structure_config.json"
STR_MODEL = "../pubtables1m_structure_detr_r18.pth"

# DEVICE = "mps"
DEVICE = "cpu"

IMG = "../../38a20dc8d2104cb8d0bf4797002f4d67_00a891b6d01dc5e4fa1cbd573aa0b11a.webp"
TOKENS = "table-transformer-tokens.json"


# TODO: Remove and use the one in vanillatils
def read_json(input_path):
    with open(input_path) as f:
        data = json.load(f)

    return data


if __name__ == "__main__":
    tokens = read_json(TOKENS)

    pipe = TableExtractionPipeline(
        det_config_path=DET_CONFIG,
        det_model_path=DET_MODEL,
        det_device=DEVICE,
        str_config_path=STR_CONFIG,
        str_model_path=STR_MODEL,
        str_device=DEVICE,
    )

    with Image.open(IMG) as img:
        extracted_tables = pipe.recognize(
            img=img,
            tokens=tokens,
            out_objects=False,
            out_cells=False,
            out_html=False,
            out_csv=True,
        )

    # print(extracted_tables)
    print(extracted_tables["csv"][0])
