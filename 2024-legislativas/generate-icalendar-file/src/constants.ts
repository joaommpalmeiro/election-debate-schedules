import { resolve } from "node:path";
import type { HeaderAttributes } from "ics";

export const HEADER: HeaderAttributes = {
  calName: "Eleições Legislativas 2024: Debates",
};

export const OUTPUT_FILENAME = resolve(__dirname, "../../data.ics");
