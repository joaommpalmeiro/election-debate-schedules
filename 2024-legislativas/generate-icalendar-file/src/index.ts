import { writeFileSync } from "node:fs";
import { createEvents } from "ics";
import type { EventAttributes } from "ics";

import SCHEDULE from "../../data.json";
import { HEADER, OUTPUT_FILENAME } from "./constants";
import { generateTitle, listify, pluralize } from "./utils";

function main() {
  const events: EventAttributes[] = SCHEDULE.map((debate) => {
    const title = generateTitle(debate.parties);

    const prefix = pluralize(debate.channels.length, "Canal", "Canais");
    const description = `${prefix}: ${listify(debate.channels)}`;

    const start = new Date(debate.start).getTime();
    const end = new Date(debate.end).getTime();

    const event = { title, description, start, end };

    return event;
  });

  const { value: output } = createEvents(events, HEADER);
  // console.log(output);

  if (output) {
    writeFileSync(OUTPUT_FILENAME, output);

    // biome-ignore lint/suspicious/noConsoleLog: to confirm the creation of the data.ics file and its path
    console.log(`${OUTPUT_FILENAME} ✓`);
  }
}

main();
