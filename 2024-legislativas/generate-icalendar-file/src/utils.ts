// TODO: Move to utility package
const listFormatter = new Intl.ListFormat("pt-PT", { style: "long", type: "conjunction" });
export function listify(values: string[]): string {
  return listFormatter.format(values);
}

// TODO: Move to utility package
const pluralRules = new Intl.PluralRules("pt-PT");
export function pluralize(count: number, singular: string, plural: string): string {
  const rule = pluralRules.select(count);

  switch (rule) {
    case "one":
      return singular;
    case "other":
      return plural;
    default:
      throw new Error(`Unknown rule: ${rule}`);
  }
}

export function generateTitle(parties: string[], sep = " / "): string {
  const title = parties.join(sep);

  return title;
}
