# Notes

- https://github.com/adamgibbons/ics/tree/v3.7.2?tab=readme-ov-file#api
- https://github.com/adamgibbons/ics/blob/v3.7.2/src/pipeline/format.js#L15
- https://github.com/adamgibbons/ics/blob/v3.7.2/src/index.js#L35
- https://support.google.com/calendar/answer/37118
- https://stackoverflow.com/a/69032498
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/ListFormat
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/PluralRules
- https://v8.dev/features/intl-pluralrules
- https://github.com/microsoft/TypeScript/issues/50381
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/ListFormat/format
- https://2ality.com/2019/12/intl-pluralrules.html
- https://github.com/adamgibbons/ics/blob/v3.7.2/index.d.ts#L1
- https://github.com/adamgibbons/ics/blob/v3.7.2/src/utils/format-date.js
- https://www.cne.pt/content/eleicoes-para-assembleia-da-republica-2024
- https://www.cne.pt/sites/default/files/dl/eleicoes/2024_ar/lista_candidatos/ar2024_sorteio_candidaturas_para_boletim_de_voto.pdf
- https://www.cne.pt/content/partidos-politicos-1
- https://icalendar.org/iCalendar-RFC-5545/3-3-5-date-time.html
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/getTime
- `npm install just-capitalize` + `const title = parties.length === 1 ? capitalize(parties[0]) : parties.join(sep);`

## Commands

```bash
npm install ics && npm install -D @biomejs/biome sort-package-json jiti npm-run-all2
```

```bash
npm install -D "@types/node@$(cat .nvmrc | cut -d . -f 1-2)"
```

```bash
rm -rf node_modules/ && npm install
```
