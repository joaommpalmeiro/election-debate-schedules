# generate-icalendar-file

Generate an iCalendar file from a JSON schedule.

## Development

Install [fnm](https://github.com/Schniz/fnm) (if necessary).

```bash
fnm install && fnm use && node --version && npm --version
```

```bash
npm install
```

```bash
npm run dev
```

```bash
npm run lint
```

```bash
npm run format
```

Validate the generated iCalendar file in [iCalendar Validator](https://icalendar.org/validator.html).
