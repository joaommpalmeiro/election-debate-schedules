# Notes

- https://github.com/jaidedai/easyocr
- https://www.jaided.ai/easyocr/: `"pt"`
- https://www.jaided.ai/easyocr/documentation/
- https://github.com/JaidedAI/EasyOCR/blob/v1.7.1/easyocr/easyocr.py#L427
- https://github.com/JaidedAI/EasyOCR/blob/v1.7.1/easyocr/easyocr.py#L440
- https://github.com/microsoft/table-transformer/blob/16d124f616109746b7785f03085100f1f6247575/docs/INFERENCE.md#pipeline-input-formats
- https://www.jaided.ai/easyocr/tutorial/

## References

- https://www.rtp.pt/noticias/pais/eleicoes-legislativas-o-calendario-dos-debates-televisivos_i1547247

## Commands

```bash
pipenv install --skip-lock easyocr==1.7.1 && pipenv install --dev --skip-lock ruff==0.1.11 codespell==2.2.6
```

```bash
pipenv --rm
```

```bash
pipenv run codespell --help
```
