import easyocr

IMG = "38a20dc8d2104cb8d0bf4797002f4d67_00a891b6d01dc5e4fa1cbd573aa0b11a.webp"

LANG = ["pt"]
OUTPUT_FMT = "json"


if __name__ == "__main__":
    reader = easyocr.Reader(LANG)

    # result = reader.readtext(IMG, output_format=OUTPUT_FMT)
    result = reader.readtext(IMG, output_format=OUTPUT_FMT, detail=0, paragraph=True)

    print(result)
