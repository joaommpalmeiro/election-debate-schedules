import json

import easyocr

IMG = "38a20dc8d2104cb8d0bf4797002f4d67_00a891b6d01dc5e4fa1cbd573aa0b11a.webp"
LANG = ["pt"]


# TODO: Remove and use the one in vanillatils
def write_json(data, output_path, add_newline=True):
    with open(output_path, "w", encoding="utf-8") as f:
        json.dump(data, f, ensure_ascii=False, indent=2)

        if add_newline:
            f.write("\n")


if __name__ == "__main__":
    reader = easyocr.Reader(LANG)
    results = reader.readtext(IMG, width_ths=0.03)

    tokens = []
    for index, res in enumerate(results):
        tokens.append(
            {
                "bbox": list(
                    map(int, [res[0][0][0], res[0][0][1], res[0][2][0], res[0][2][1]])
                ),
                "text": res[1],
                "flags": 0,
                "span_num": index,
                "line_num": 0,
                "block_num": 0,
            }
        )
    print(tokens)

    write_json(tokens, "table-transformer-tokens.json")
