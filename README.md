# election-debate-schedules

JSON and iCalendar files for Portuguese election debate schedules.

| Election                                        | JSON                                     | iCalendar                              | Source                                                                                                                        | Version                            |
| ----------------------------------------------- | ---------------------------------------- | -------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------- | ---------------------------------- |
| [Eleições Legislativas 2024](2024-legislativas) | [data.json](2024-legislativas/data.json) | [data.ics](2024-legislativas/data.ics) | [RTP](https://www.rtp.pt/noticias/pais/eleicoes-legislativas-o-calendario-dos-debates-televisivos_i1547247)                   | `publicado 31 Janeiro 2024, 11:10` |
| [Eleições Europeias 2024](2024-europeias)       | [data.json](2024-europeias/data.json)    | [data.ics]()                           | [RTP](https://www.rtp.pt/noticias/politica/europeias-partidos-aceitam-proposta-das-televisoes-para-debates-a-quatro_n1570038) | `atualizado 9 Maio 2024, 08:24`    |
